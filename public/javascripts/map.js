var map = L.map('main_map').setView([32.5027, -117.00371], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap </a> contributors',
}).addTo(map);


L.marker([32.53006, -116.98724], {title:'TecNM'}).addTo(map);
L.marker([32.53203, -116.96180], {title:'FCA'}).addTo(map);


$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
            //http://127.0.0.1:3000/api/bicicletas
        });
    }
})


//script(src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js')
//se agrego a la vista layout ya que no reconocia AJAX